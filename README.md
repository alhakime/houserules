##Repository
- Hakim create repository "houserules"
- Hakim and ernesto clone git houserules

##Branching
- Hakim create branch 'page/css' and 'page/houseRules'
- Ernesto create branch 'feature/notice' and 'detailfeature/notice'

##workflow git
- Hakim commit 'page/houseRules' 
- Ernesto pull 'page/houseRules'
- Ernesto commit 'feature/notice'
- Hakim commit 'feature/notice'
- Ernesto commit 'detailfeature/notice'
- Hakim pull 'detailfeature/notice'
- Ernesto pull request 'detailfeature/notice' to 'feature/notice'
- Hakim review pull request 'detailfeature/notice'
- pull request 'detailfeature/notice' to 'feature/notice' accept
- Ernesto pull request 'feature/notice' to 'develop'
- Hakim review pull request 'detailfeature/notice'
- pull request 'feature/notice' to 'develop' declined
- Hakim commit 'page/css'
- Hakim commit 'page/css'
- Ernesto pull request 'feature/notice' to 'develop'
- Hakim review pull request 'feature/notice'
- pull request 'feature/notice' to 'develop' accept but conflict
- Hotfix branch
- Delete hotfix branch
- Ernesto fork and fix the conflict on pull request 'feature/notice' to 'develop'
- Merge 'feature/notice' and 'develop'
- Hakim merge develop to 'page/css'
- Hakim pull request 'page/houseRules' to 'develop'
- Ernesto review pull request 'page/houseRules'
- pull request 'page/houseRules' to 'develop' declined
- Hakim edit and commit 'page/houseRules'
- Hakim pull request 'page/houseRules' to 'develop'
- pull request 'page/houseRules' to 'develop' accept
- Merge 'page/houseRules' and 'develop'
- Release branch 0.1.0
- revert release branch 0.1.0
- Hakim commit 'page/css'
- Hakim Pull Request 'page/css' to 'develop'
- Ernesto review Pull Request 'page/css'
- Pull Request 'page/css' to 'develop' accept
- Release branch 0.2.0
- Finish release 0.2.0